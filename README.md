# text-viewer

Simple example app that demonstrates read-only usage of the [Flywheel extension SDK](https://cdn.flywheel.io/docs/extension/latest/)

## What it does

The app will read the text content of a file and write it to a preformatted content element.

## How it's deployed

The index.html file is uploaded to a Google Cloud Storage bucket, accessible at https://storage.googleapis.com/flywheel-extension-apps/apps/text-viewer/1.0.0/index.html. This URL is used to register the application within Flywheel.

## Registering the app

To register this application in Flywheel as-is, go to the Applications page under the Admin section. You'll require site admin privileges. Click the Register New Application button.

![site applications](./img/000_site_applications.png)

Fill out the dialog with the app name, URL, and file type matcher. Click the Register button.

![register dialog](./img/001_app_registration.png)

## Testing the app

Now go to a text file within Flywheel and click the View action icon.

![view file](./img/002_view_text_file.png)

You should see the viewer dialog load and the app render the file name along with the content.

![viewer dialog](./img/003_viewer_dialog.png)
